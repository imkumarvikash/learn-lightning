public with sharing class CarSearchByType {
    
    @AuraEnabled
    public static List<Car__c> getCars(String carTypeId){
        // If all type is selected
        if(carTypeId.trim().equals('')){
            return [SELECT id, Name, Picture__c, Contact__r.Name, Geolocation__latitude__s, Geolocation__longitude__s
            FROM Car__c
            WHERE Available_For_Rent__c = true];
        }
        // Else if specific type is selected
        else{
            return [SELECT id, Name, Picture__c, Contact__r.Name, Geolocation__latitude__s, Geolocation__longitude__s
            FROM Car__c
            WHERE Available_For_Rent__c = true AND Car_Type__c = :carTypeId];
        }
    }
}
