({
    doFormSubmit : function(component, event, helper) {
        var carTypeId = event.getParam('carTypeId');
        console.log("Selected Car Type Id: " + carTypeId);
        var carSearchResultComp = component.find("carSearchResult");
        var cars = carSearchResultComp.searchCars(carTypeId);
        console.log("Aura Method Result: " + cars);
    }
})
