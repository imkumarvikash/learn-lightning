({
    doInit : function(component, event, helper) {
        helper.onSearch(component, helper);
    },

    doSearch: function(component, event, helper) {
        //get arguments passed to the aura:method
        var params = event.getParam("arguments");
        console.log('Search Parameters: ' + params);
        if(params){
            component.set("v.carTypeId", params.carTypeId);
            helper.onSearch(component, helper);
        }
    }
})
