({
    doInit : function(component, event, helper){
        //Check if create action is available for current user
        var createCarRecord = $A.get("e.force:createRecord");
        if(createCarRecord) {
            component.set("v.newDisabled", false);
            console.log("New Button Enabled");
        } else {
            component.set("v.newDisabled", true);
            console.log("New Button Disabled");
        }
        
        //Set Car Type List
        helper.setCarTypeList(component, helper);
    },
    
    onSearchClick : function(component, event, helper) {
        var searchEvent = component.getEvent("searchFormSubmit");
        searchEvent.setParams({
            "carTypeId" : component.find("carType").get("v.value")
        });
        searchEvent.fire();
	},
    
    createCarTypeRecord : function(component, event, helper) {
        console.log("Controller to create new record called")
        var createCarType = $A.get("e.force:createRecord");
        createCarType.setParams({
            "entityApiName": "Car_Type__c",
        });
        createCarType.fire();
    },
})