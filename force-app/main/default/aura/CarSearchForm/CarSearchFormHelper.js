({
	setCarTypeList : function(component, helper) {
		//component.set("v.listCarTypes", ['Hatch', 'Small Sedan', 'Mid Sedan', 'Luxury Sedan', 'Compact SUV', 'Full-size SUV']);
		//component.set("v.listCarTypes", component.getCarTypes());
        
        // var action = component.get('c.getCarTypes');
        
        // //Set up the callback
        // action.setCallback(this, function(actionResult){
        //     component.set('v.listCarTypes', actionResult.getReturnValue());
        // });
        // $A.enqueueAction(action);

        helper.callServer(component, "c.getCarTypes", function(actionResult) {
            component.set("v.listCarTypes", actionResult);
        });
	}
})